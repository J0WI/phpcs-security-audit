FROM php:7.2-cli-stretch

# PHP Composer requires zip
RUN apt-get update
RUN apt-get install -y zip

# Create php user and group
RUN groupadd -g 1000 php
RUN useradd -u 1000 -g 1000 -N -m php

# Ensure we don't hit PHP memory limits on larger files
RUN echo 'memory_limit=-1' > /usr/local/etc/php/conf.d/memory.ini

# The php user below doesn't have permission to create a file in /etc/ssl/certs, this
# RUN command creates a file that the analyzer can add additional ca certs to trust.
RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chown root:php /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem && \
    chmod g+w /etc/ssl/certs/ca-cert-additional-gitlab-bundle.pem

USER php:php
WORKDIR /home/php/

# Install PHP Composer then PHP CodeSniffer using Composer
ADD --chown=php:php https://getcomposer.org/download/1.6.5/composer.phar .
COPY --chown=php:php composer.json .
RUN ["php", "composer.phar", "install"]

# Install phpcs-security-audit and add standards to PHP CodeSniffer
ADD --chown=php:php https://github.com/FloeDesignTechnologies/phpcs-security-audit/archive/2.0.0.zip .
RUN ["unzip", "2.0.0.zip"]
RUN ["mv", "phpcs-security-audit-2.0.0/Security", "vendor/squizlabs/php_codesniffer/src/Standards/"]
RUN ["mv", "phpcs-security-audit-2.0.0/example_base_ruleset.xml", "."]
RUN ["mv", "phpcs-security-audit-2.0.0/example_drupal7_ruleset.xml", "."]
RUN ["mv", "phpcs-security-audit-2.0.0/tests.php", "."]
ADD --chown=php:php ruleset.xml .

# GitLab CI wants no entry point
ENTRYPOINT []
COPY /analyzer /
CMD ["/analyzer", "run"]
