module gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit/v2

require (
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli v1.22.1
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.9.1
	gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit v1.3.0
)

go 1.13
